/*
 * Copyright 2018 Tantowi Mustofa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tantowi.maven.tomcat;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Utility class
 * 
 * @author Tantowi Mustofa (ttw@tantowi.com)
 */
class Utility
{

	/**
	 * copy content of a file to output stream
	 * @param path file to copy
	 * @param os output stream to copy to
	 * @throws IOException IOException on error
	 */
	static void copy(Path path, OutputStream os) throws IOException
	{
		try (InputStream fis = Files.newInputStream(path))
		{
			copy(fis, os);
		}
	}
	
	/**
	 * copy from InputStream to OutputStream until end of stream detected
	 * @param is input stream to copy
	 * @param os output stream to copy to
	 * @throws IOException IOException on IO error
	 */
	static void copy(InputStream is, OutputStream os) throws IOException
	{
		if (is == null) return;
		if (os == null) return;
		
		int i;
		byte[] buffer = new byte[1024];
		while ((i = is.read(buffer)) > -1) os.write(buffer, 0, i);
	}
	
	/**
	 * read an input stream to file
	 * @param is input stream to read from
	 * @return string content of the stream
	 * @throws java.io.IOException On IO error
	 */
	static String read (InputStream is) throws IOException
	{
		if (is == null) return "";
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		copy (is, os);
		return os.toString("UTF-8");
	}

	/**
	 * encode for Url
	 * @param str string to encode
	 * @return encoded string
	 */
	static String UrlEncode (String str)
	{
		try
		{
			return URLEncoder.encode(str, "UTF-8");
		}
		catch (UnsupportedEncodingException ex)
		{
			return str;
		}
	}

	/**
	 * build tomcat url
	 * @param serverUrl server Url
	 * @param command tomcat command to perform
	 * @param path context path 
	 * @param version version
	 * @param forceUpdate force update on deploy
	 * @return full url to tomcat end point API
	 */
	static URL buildTomcatUrl (String serverUrl, String command, String path, String version, boolean forceUpdate) throws MojoFailureException
	{
		try
		{
			if (!path.startsWith("/")) path = "/" + path;
			
			StringBuilder sb = new StringBuilder();
			sb.append(serverUrl);
			sb.append("/manager/text/");
			sb.append(command.trim());
			sb.append("?path=");
			sb.append(Utility.UrlEncode(path));
			if (!version.isEmpty()) 
			{
				sb.append("&version=");
				sb.append(Utility.UrlEncode(version));
			}
			if (forceUpdate) sb.append("&update=true");
			return new URL (sb.toString());
		}
		catch (MalformedURLException ex)
		{
			throw new MojoFailureException(ex.getMessage(), ex);
		}
	}

	
	/**
	 * find credential from credential list
	 * @param servers credential list
	 * @param id id to search for
	 * @return credential object
	 */
	static org.apache.maven.settings.Server findCredential (List<org.apache.maven.settings.Server> servers, String id)
	{
		for (int i = 0; i < servers.size(); i++)
		{
			org.apache.maven.settings.Server cred = servers.get(i);
			if (cred.getId().equals(id)) return cred;
		}
		return null;
	}
	
	/**
	 * check parameter : servers
	 * @param servers servers to check
	 * @return error list (empty if no errors detected)
	 */
	static String[] checkParameter(List<Server> servers)
	{
		List<String> errors = new ArrayList<>();

		if (servers==null || servers.isEmpty()) 
		{
			errors.add("No server defined in <servers> section");
			return errors.toArray(new String[0]);
		}
		
		for (int i = 0; i < servers.size(); i++)
		{
			Server server = servers.get(i);

			String id = server.getId();
			if (id==null || id.isEmpty()) errors.add("Parameter <id> not specified for server #" + String.valueOf(i+1));

			String url = server.getUrl();
			if (url==null || url.isEmpty()) errors.add("Parameter <url> not specified for server #" + String.valueOf(i+1));

			String path = server.getPath();
			if (path==null || path.isEmpty()) errors.add("Parameter <path> not specified for server #" + String.valueOf(i+1));
			
			String replace = server.getReplace();
			if (replace==null || replace.isEmpty()) server.setReplace("no");
		}
		
		return errors.toArray(new String[0]);
	}
	
}
