/*
 * Copyright 2018 Tantowi Mustofa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tantowi.maven.tomcat;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugin.logging.Log;

/**
 * goal tomcat:help
 * 
 * @author Tantowi Mustofa (ttw@tantowi.com)
 */
@Mojo(name = "help", defaultPhase = LifecyclePhase.NONE)
public class HelpMojo extends AbstractMojo
{
	//@Parameter(property = "outputDir", defaultValue = "${project.build.directory}", required = true)
	//private File outputDir;

	/**
	 * execute goal tomcat:help
	 *
	 */
	@Override
	public void execute()
	{
		Log log = getLog();
		log.info("Plugin: com.tantowi.maven:tomcat-maven-plugin");
		log.info("Available goals:");
		log.info("  tomcat:deploy    - deploy project war file to remote tomcat server.");
		log.info("  tomcat:undeploy  - undeploy project from remote tomcat server.");
		log.info("  tomcat:start     - start application on remote tomcat server.");
		log.info("  tomcat:stop      - stop application on remote tomcat server.");
		log.info("  tomcat:reload    - reload application on remote tomcat server.");
		log.info("  tomcat:help      - display this help.");
		log.info("");
		log.info("Copyright 2018 M Tantowi Mustofa (ttw@tantowi.com)");
	}
}
