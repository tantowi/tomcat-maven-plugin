/*
 * Copyright 2018 Tantowi Mustofa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tantowi.maven.tomcat;

import java.io.*;
import java.net.URL;
import java.net.UnknownHostException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Execute;

/**
 * goal tomcat:deploy
 * 
 * @author Tantowi Mustofa (ttw@tantowi.com)
 */
@Mojo(name = "deploy", defaultPhase = LifecyclePhase.DEPLOY)
@Execute(phase = LifecyclePhase.DEPLOY)
public class DeployMojo extends AbstractMojo
{
	@Parameter(defaultValue = "${project.name}", readonly = true)
	private String projectName;

	@Parameter(defaultValue="${project.version}", readonly=true)
	private String projectVersion;

	@Parameter(defaultValue="${project.packaging}", readonly=true)
	private String projectPackaging;
	
	@Parameter(defaultValue="${project.build.directory}", readonly=true)
	private File outputDirectory;

	@Parameter(defaultValue="${project.build.finalName}", readonly=true)
	private String finalName;

	@Parameter(defaultValue="${settings.servers}", readonly=true)
	private List<org.apache.maven.settings.Server> credentials;
	
	@Parameter(property="servers", defaultValue="", required=true, readonly=true)
	private List<Server> servers;
	
	/**
	 * execute the goal : tomcat:deploy
	 * deploy the output war file to remote tomcat server
	 *
	 * https://stackoverflow.com/questions/1033154/when-to-use-mojoexecutionexception-vs-mojofailureexception-in-maven
	 * @throws MojoExecutionException on build failure
	 * @throws MojoFailureException on plugin failure
	 */
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException
	{
		Log log = getLog();
		log.info("Deploy " + projectName + " version " + projectVersion);
		
		String[] errors = Utility.checkParameter(servers);
		if (errors.length > 0)
		{
			for (String error : errors) log.error("Error: " + error);
			throw new MojoFailureException("Error(s) in <servers> parameter");
		}
		
		Path warfile = Paths.get(outputDirectory + File.separator + finalName + "." + projectPackaging);
		if (!Files.exists(warfile)) throw new MojoExecutionException("Cannot find war file: " + warfile.toString());

		for (Server srv : this.servers) deployServer(warfile, srv);
	}

	/**
	 * deploy to a server
	 * @param warfile war file to upload
	 * @param server server to upload to
	 * @throws MojoExecutionException on build failure
	 * @throws MojoFailureException on plugin failure
	 */
	private void deployServer (Path warfile, Server server) throws MojoExecutionException, MojoFailureException
	{
		Log log = getLog();
		log.info("Deploy to server-id " + server.getId() + ": " + server.getUrl());
		
		URL url = Utility.buildTomcatUrl(server.getUrl(), "deploy", server.getPath(), projectVersion, server.isReplace());
		try (SimpleHttp http = new SimpleHttp(url, "PUT"))
		{
			org.apache.maven.settings.Server cred = Utility.findCredential(credentials, server.getId());
			if (cred != null)
			{
				String username = cred.getUsername();
				String password = cred.getPassword();
				http.setAuthentication(username, password);
			}

			SimpleHttpResponse response = http.upload(warfile);
			if (response.getCode() != 200)
			{
				String msg = "ERROR " + String.valueOf(response.getCode()) + ": " + response.getBody();
				log.error(msg);
				throw new MojoExecutionException(msg);
			}
			
			if (response.getBody().startsWith("FAIL"))
			{
				log.error(response.getBody());
				throw new MojoExecutionException(response.getBody());
			}

			// OK
			log.info(response.getBody());
		}
		catch (UnknownHostException ex)
		{
			String msg = "Unknown Host: " + ex.getMessage();
			log.error(msg);
			throw new MojoFailureException(msg, ex);
		}
		catch (IOException ex)
		{
			Throwable th = ex;
			while (th.getCause() != null) th = th.getCause();
			log.error(th.getMessage());
			throw new MojoFailureException(th.getMessage(), th);
		}
	}
	
		
}
