/*
 * Copyright 2018 Tantowi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tantowi.maven.tomcat;

/**
 * Server Class
 * @author Tantowi
 */
public class Server
{
	private String id;
	private String url;
	private String path;
	private String replace;

	/**
	 * get server-id
	 * @return server-id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * set server-id
	 * @param id server-id
	 */
	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * get server-url
	 * @return server-url
	 */
	public String getUrl()
	{
		return url;
	}

	/**
	 * set server-url
	 * @param url server-url
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

	/**
	 * get server-path
	 * @return server-path
	 */
	public String getPath()
	{
		return path;
	}

	/**
	 * set server-path
	 * @param path server-path
	 */
	public void setPath(String path)
	{
		this.path = path;
	}

	/**
	 * get server-replace
	 * @return "yes" if existing application should be replaced
	 */
	public String getReplace()
	{
		return replace;
	}

	/**
	 * isreplace
	 * 
	 * @return should replace installed application ?
	 */
	public boolean isReplace()
	{
		return replace.toLowerCase().equals("yes");
	}
	
	
	/**
	 * set server-replace
	 * @param replace set to "yes" if existing application should be replaced
	 */
	public void setReplace(String replace)
	{
		this.replace = replace.toLowerCase();
	}
	
}
