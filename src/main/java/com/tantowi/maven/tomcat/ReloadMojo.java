/*
 * Copyright 2018 Tantowi Mustofa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tantowi.maven.tomcat;

import java.io.*;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;

/**
 * goal tomcat:reload
 * 
 * @author Tantowi Mustofa (ttw@tantowi.com)
 */
@Mojo(name = "reload", defaultPhase = LifecyclePhase.NONE)
public class ReloadMojo extends AbstractMojo
{
	@Parameter(defaultValue = "${project.name}", readonly = true)
	private String projectName;

	@Parameter(defaultValue="${project.version}", readonly=true)
	private String projectVersion;

	@Parameter(defaultValue="${settings.servers}", readonly=true)
	private List<org.apache.maven.settings.Server> credentials;
	
	@Parameter(property="servers", defaultValue="", required=true, readonly=true)
	private List<Server> servers;
	
	/**
	 * execute the goal : tomcat:reload
	 * reload the application
	 *
	 * @throws org.apache.maven.plugin.MojoExecutionException on execution failure
	 * @throws org.apache.maven.plugin.MojoFailureException on mojo failure
	 */
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException
	{
		Log log = getLog();
		log.info("Reload " + projectName + " version " + projectVersion);
		
		String[] errors = Utility.checkParameter(servers);
		if (errors.length > 0)
		{
			for (String error : errors) log.error("Error: " + error);
			throw new MojoFailureException("Error(s) in <servers> parameter");
		}
		
		for (Server srv : this.servers) reloadServer(srv);
	}

	/**
	 * reload a server
	 * @param server server to reload
	 * @throws MojoExecutionException on build failure
	 * @throws MojoFailureException on plugin failure
	 */
	private void reloadServer (Server server) throws MojoExecutionException, MojoFailureException
	{
		Log log = getLog();
		log.info("Reloading server-id " + server.getId() + ": " + server.getUrl());
		
		URL url = Utility.buildTomcatUrl(server.getUrl(), "reload", server.getPath(), this.projectVersion, false);

		try (SimpleHttp http = new SimpleHttp(url, "GET"))
		{
			org.apache.maven.settings.Server cred = Utility.findCredential(credentials, server.getId());
			if (cred != null)
			{
				String username = cred.getUsername();
				String password = cred.getPassword();
				http.setAuthentication(username, password);
			}
			
			SimpleHttpResponse response = http.connect();
			if (response.getCode() != 200)
			{
				String msg = "ERROR " + String.valueOf(response.getCode()) + ": " + response.getBody();
				log.error(msg);
				throw new MojoExecutionException(msg);
			}
			
			if (response.getBody().startsWith("FAIL"))
			{
				log.error(response.getBody());
				throw new MojoExecutionException(response.getBody());
			}
			
			// OK
			log.info(response.getBody());
		}
		catch (UnknownHostException ex)
		{
			String msg = "Unknown Host: " + ex.getMessage();
			log.error(msg);
			throw new MojoFailureException(msg, ex);
		}
		catch (IOException ex)
		{
			Throwable th = ex;
			while (th.getCause() != null) th = th.getCause();
			log.error(th.getMessage());
			throw new MojoFailureException(th.getMessage(), th);
		}
	}
}
