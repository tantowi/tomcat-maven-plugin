/*
 * Copyright 2018 Tantowi Mustofa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tantowi.maven.tomcat;

/**
 * SimpleHttpResponse class
 * 
  * @author Tantowi Mustofa (ttw@tantowi.com)
*/
public class SimpleHttpResponse
{
	private int code = 0;
	private String message = "";
	private String body = "";

	/**
	 * get Response Code
	 * @return response code
	 */
	public int getCode()
	{
		return code;
	}

	/**
	 * set Response Code
	 * @param code response code
	 */
	public void setCode(int code)
	{
		this.code = code;
	}

	/**
	 * get Response Message
	 * @return response message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * set Response Message
	 * @param message response message
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * get Response Body
	 * @return response body
	 */
	public String getBody()
	{
		return body;
	}

	/**
	 * set Response Body
	 * @param body response body
	 */
	public void setBody(String body)
	{
		this.body = body;
	}
	
	
	
}
