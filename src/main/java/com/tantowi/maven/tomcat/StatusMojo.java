/*
 * Copyright 2018 Tantowi Mustofa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tantowi.maven.tomcat;

import java.io.File;
import java.util.List;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * goal tomcat:status
 * 
 * @author Tantowi Mustofa (ttw@tantowi.com)
 */
@Mojo( name = "status", defaultPhase = LifecyclePhase.NONE )
public class StatusMojo extends AbstractMojo
{
	@Parameter (defaultValue="${project.basedir}", readonly=true)
   private File baseDirectory;

	@Parameter (defaultValue="${project.version}", readonly=true)
	private String projectVersion;

	@Parameter(defaultValue="${project.build.directory}", readonly=true)
   private File outputDirectory;

	@Parameter(defaultValue="${project.build.finalName}.${project.packaging}", readonly=true)
	private String finalName;

	@Parameter(defaultValue="${settings.interactiveMode}", readonly=true)
	private boolean interactiveMode;
	
	@Parameter(defaultValue="${maven.home}", readonly=true)
	private File mavenHome;
	
	@Parameter(defaultValue="${user.home}", readonly=true)
	private File userHome;

	@Parameter(defaultValue="${settings.servers}", readonly=true)
	private List<org.apache.maven.settings.Server> credentials;

	@Parameter(property="servers", defaultValue="", required=false, readonly=true)
	private List<Server> servers;

	
	/**
	 * execute the goal tomcat:status
	 * @throws org.apache.maven.plugin.MojoExecutionException on execution failure
	 * @throws org.apache.maven.plugin.MojoFailureException on mojo failure
	 */
	@Override
   public void execute() throws MojoExecutionException, MojoFailureException
   {
		Log log = getLog();
		String[] errors = Utility.checkParameter(servers);
		if (errors.length > 0)
		{
			for (String error : errors) log.error(error);
			throw new MojoFailureException("Error(s) in <servers> parameter");
		}
		
		try
		{
			System.out.println("");
			System.out.println("Welcome to tomcat-maven-plugin");
			System.out.println("Maven Home       : " + mavenHome.getPath());
			System.out.println("User Home        : " + userHome.getPath());
			System.out.println("Interactive Mode : " + (interactiveMode ? "yes" : "no"));
			System.out.println("");
			System.out.println("Project Base Dir : " + baseDirectory.getPath());
			System.out.println("Project Version  : " + projectVersion);
			System.out.println("Output Directory : " + outputDirectory.getPath());
			System.out.println("Final Name       : " + finalName);

			//System.out.println("");
			//System.out.println("Credentials:");
			//for (int i = 0; i < this.credentials.size(); i++)
			//{
			//	org.apache.maven.settings.Server cred = this.credentials.get(i);
			//	String id = cred.getId();
			//	String user = cred.getUsername();
			//	String pass = cred.getPassword();
			//	System.out.println("  #" + String.valueOf(i));
			//	System.out.println("  Id    : " + id);
			//	System.out.println("  User  : " + user);
			//	System.out.println("  Pass  : " + pass);
			//}
			
			System.out.println("");
			for (int i = 0; i < this.servers.size(); i++)
			{
				Server srv = this.servers.get(i);
				System.out.println("Server #" + String.valueOf(i+1));
				System.out.println("  Id      : " + srv.getId());
				System.out.println("  Url     : " + srv.getUrl());
				System.out.println("  Path    : " + srv.getPath());
				System.out.println("  Replace : " + srv.getReplace());
				
				org.apache.maven.settings.Server cred = Utility.findCredential(credentials, srv.getId());
				if (cred != null) System.out.println("  Credential found (Username:" + cred.getUsername() + ")");
				else System.out.println("  Credential not found");
			}
			System.out.println("");
		}
		catch (Exception ex)
		{
			log.error(ex.getMessage());
		}
	}  
}
