/*
 * Copyright 2018 Tantowi Mustofa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tantowi.maven.tomcat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

/**
 * SimpleHttp class
 * 
 * @author Tantowi Mustofa (ttw@tantowi.com)
 */
class SimpleHttp implements AutoCloseable
{
	private HttpURLConnection con = null;

	/**
	 * constructor
	 * @param url url to connect to
	 * @param method http method to perform
	 * @throws java.io.IOException on IO error
	 */
	SimpleHttp (URL url, String method) throws IOException
	{
		con = (HttpURLConnection) url.openConnection();
		con.setInstanceFollowRedirects(true);
		con.setReadTimeout(60000);
		con.setConnectTimeout(60000);
		con.setRequestProperty("User-Agent", "Tomcat Maven Plugin");

		setRequestMethod(method);
	}

	/**
	 * set request method
	 * 
	 * @param method method to perform
	 * @throws IOException on IO error
	 */
	private void setRequestMethod (String method) throws IOException
	{
		if (con==null) return;

		method = method.toUpperCase();
		try
		{
			con.setRequestMethod(method);
			if (method.equals("POST") || method.equals("PUT"))
			{
				con.setDoOutput(true);
				con.setDoInput(true);
			}
			else
			{
				con.setDoOutput(false);
				con.setDoInput(true);
			}
		}
		catch (ProtocolException ex)
		{
			throw new IOException("Unknown method: " + method);
		}
	}

	/**
	 * set request header/property
	 * 
	 * @param name header name
	 * @param value header value
	 */
	void setRequestProperty(String name, String value)
	{
		if (con==null) return;
		con.setRequestProperty(name, value);
	}

	/**
	 * set basic authentication
	 * 
	 * @param username username
	 * @param password password
	 */
	void setAuthentication(String username, String password)
	{
		if (con==null) return;
		if (username==null || username.isEmpty()) return;

		if (password==null) password = "";
		Base64.Encoder encoder = Base64.getEncoder();
		String authStr = username.trim() + ":" + password.trim();
		String authEncoded = new String(encoder.encode(authStr.getBytes()));
		con.setRequestProperty("Authorization", "Basic " + authEncoded);
	}
	
	/**
	 * connect and get response
	 * 
	 * @return SimpleHttpRespose
	 * @throws java.io.IOException on IO error
	 */
	SimpleHttpResponse connect() throws IOException
	{
		if (con==null) return null;
		
		con.connect();
		return getResponse();
	}
	
	
	/**
	 * upload the file and get response
	 * 
	 * @param path file to upload
	 * @return SimpleHttpResponse
	 * @throws IOException on IO errors
	 */
	SimpleHttpResponse upload(Path path) throws IOException
	{
		if (con==null) return null;

		con.setFixedLengthStreamingMode(Files.size(path));
		con.connect();
		try (OutputStream fos = con.getOutputStream())
		{
			Utility.copy(path, fos);
		}

		return getResponse();
	}
	
	/**
	 * getResponse
	 * 
	 * @return SimpleHttpResponse
	 */
	private SimpleHttpResponse getResponse() throws IOException
	{
		if (con==null) return null;
		
		int responseCode = con.getResponseCode();
			
		String responseMessage = con.getResponseMessage();
		if (responseMessage == null) responseMessage = "";
			
		String responseBody;
		if (responseCode==200)
		{
			InputStream in = con.getInputStream();
			responseBody = Utility.read(in);
			if (in != null) in.close();
		}
		else
		{
			InputStream in = con.getErrorStream();
			responseBody = Utility.read(in);
			if (in != null) in.close();
		}
		
		SimpleHttpResponse res = new SimpleHttpResponse();
		res.setCode(responseCode);
		res.setMessage(responseMessage);
		res.setBody(responseBody);
		return res;
	}

	/**
	 * close/disconnect
	 */
	@Override
	public void close()
	{
		if (con==null) return;
		con.disconnect();
	}

	
}
