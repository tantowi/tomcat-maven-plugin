# tomcat-maven-plugin



tomcat-maven-plugin is a maven plugin to deploy application to remote tomcat server





### Usage

```xml
<build>
   <plugins>

      <plugin>
         <groupId>com.tantowi.maven</groupId>
         <artifactId>tomcat-maven-plugin</artifactId>
         <version>1.0.0</version>
      </plugin>

   </plugins>
</build>
```



### Configuration



```xml
<build>
   <plugins>
         
      <plugin>
         <groupId>com.tantowi.maven</groupId>
         <artifactId>tomcat-maven-plugin</artifactId>
         <version>1.0.0</version>
         <configuration>
            <servers>
               <server>
                  <id>vm01</id>
                  <url>http://vm01.tantowi.com:8080</url>
                  <path>/</path>
                  <replace>yes</replace>
               </server>
               <server>
                  <id>vm02</id>
                  <url>http://vm02.tantowi.com:8080</url>
                  <path>/</path>
                  <replace>yes</replace>
               </server>
               <server>
                  <id>vm03</id>
                  <url>http://vm03.tantowi.com:8080</url>
                  <path>/</path>
                  <replace>yes</replace>
               </server>
            </servers>
         </configuration>
      </plugin>
       
   </plugins>
</build>
```

- id : used to locate credential in settings.xml 
- url : url to the tomcat server, including port if necessary
- path : path on server where the application will be deploy
- replace : yes - replace existing application if any. no - report an error if application already installed



#### Credential

[home]/.m2/settings.xml

```xml
<servers>

   <server>
      <id>dbcr-repo</id>
      <username>admin</username>
      <password>admin</password>
   </server>

</servers> 
```

- id : id of this credential
- username : username
- password : password



##### Run on `mvn deploy`



```xml
   <plugin>
      <groupId>com.tantowi.maven</groupId>
      <artifactId>tomcat-maven-plugin</artifactId>
      <version>1.0.0</version>
      <configuration>
         <server>
            <id>tantowi-dev</id>
            <url>http://dev.tantowi.com:8080</url>
            <path>/demo</path>
            <replace>yes</replace>
         </server>
      </configuration>
      <executions>
         <execution>
            <phase>deploy</phase>
            <goals>
               <goal>deploy</goal>
            </goals>
         </execution>
      </executions>			      
   </plugin>
```





